﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HazelcastTest
{
    public static class RandomGenerator
    {
        private static Random _randomNumberGenerator;

        static RandomGenerator()
        {
            _randomNumberGenerator = new Random();
        }

        public static int GetRamdomInteger()
        {
            var centuryBegin = new DateTime(2001, 1, 1);
            var currentDate = DateTime.Now;

            var elapsedTicks = currentDate.Ticks - centuryBegin.Ticks;
            if (elapsedTicks > int.MaxValue)
                elapsedTicks = elapsedTicks % int.MaxValue;

            return Convert.ToInt32(elapsedTicks);
        }

        public static string GetAlphabets(int digits)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var stringChars = new char[digits];
            for (var i = 0; i < digits; i++)
            {
                stringChars[i] = chars[_randomNumberGenerator.Next(chars.Length - 1)];
            }

            return new string(stringChars);
        }
    }
}
