﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HazelcastTest
{
    [TestClass]
    public class HazelcastClientTestBase
    {
        protected HttpClient[] ClientArray;
        protected HttpClient Client1;
        protected HttpClient Client2;
        protected HttpClient Client3;

        public HazelcastClientTestBase()
        {
            ClientArray = new[] {Client1, Client2, Client3};

            for (var i = 0; i < ClientArray.Length; i++)
            {
                ClientArray[i] = new HttpClient
                                 {
                                     BaseAddress =
                                         new Uri("http://hazelcast" + (i + 1) + ".azurewebsites.net/")
                                 };
                ClientArray[i].DefaultRequestHeaders.Accept.Clear();
                ClientArray[i].DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }
    }
}
