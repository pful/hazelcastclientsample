﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAppForManyClientHazelcastTest.Models;

namespace HazelcastTest
{
    [TestClass]
    public class EmployeesControllerTestsUsingThreeClients : HazelcastClientTestBase
    {
        // Basic CRUD Tests done through 'postman'

        private Employee _employee;

        [TestInitialize]
        public void InitializeAndInsertEmployee()
        {
            _employee = new Employee
                        {
                            Id = RandomGenerator.GetRamdomInteger(),
                            Name = RandomGenerator.GetAlphabets(5)
                        };

            Task.Run(
                async () => { await ClientArray[0].PostAsJsonAsync("api/Employees", _employee); })
                .Wait();
        }

        [TestCleanup]
        public void CleanupEmployee()
        {
            Task.Run(
                async () => { await ClientArray[0].DeleteAsync("api/Employees/" + _employee.Id); })
                .Wait();
        }

        [TestMethod]
        public void When_GetEmployeesFromClients_Expect_SameEmployeeCollection()
        {
            // Arrange
            var results = new string[ClientArray.Length];
            var listsOfResults = new List<Employee>[ClientArray.Length];

            // Act
            Task.Run(
                async () =>
                      {
                          for (var i = 0; i < results.Length; i++)
                          {
                              results[i] = await ClientArray[i].GetAsync("api/Employees")
                                                               .Result.Content.ReadAsStringAsync();
                              listsOfResults[i] = JsonDeserializer<Employee>.DeserializeCollection(results[i])
                                                                            .ToList();
                          }
                      })
                .Wait();

            // Assert
            var listCount = listsOfResults[0].Count;
            foreach (var i in listsOfResults)
            {
                Assert.IsTrue(i.Exists(x => x.Id == _employee.Id));
                Assert.AreEqual(listCount, i.Count);
            }
        }

        [TestMethod]
        public void When_GetEmployeeAfterInsertAndDeleteByDifferentClient_Expect_Null()
        {
            // Arrange
            var results = new HttpStatusCode[ClientArray.Length];
            Task.Run(
                async () => { await ClientArray[0].DeleteAsync("api/Employees/" + _employee.Id); })
                .Wait();

            // Act
            Task.Run(
                async () =>
                      {
                          for (var i = 0; i < results.Length; i++)
                          {
                              var getEmployeeTask = await ClientArray[i].GetAsync("api/Employee/" + _employee.Id);
                              results[i] = getEmployeeTask.StatusCode;
                          }
                      })
                .Wait();

            // Assert
            foreach (var i in results)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, i);
            }
        }

        [TestMethod]
        public void When_GetEmployeeAfterInsertAndUpdateByDifferentClient_Expect_ChangedEmployee()
        {
            // Arrange
            _employee.Name = "changedName";
            var results = new string[ClientArray.Length];
            var employees = new Employee[ClientArray.Length];

            Task.Run(
                async () => { await ClientArray[0].PutAsJsonAsync($"api/Employees/{_employee.Id}", _employee); })
                .Wait();

            // Act
            Task.Run(
                async () =>
                      {
                          for (var i = 0; i < employees.Length; i++)
                          {
                              results[i] = await ClientArray[i].GetAsync($"api/Employees/{_employee.Id}")
                                                               .Result.Content.ReadAsStringAsync();
                              employees[i] = JsonDeserializer<Employee>.DeserializeEntity(results[i]);
                          }
                      })
                .Wait();

            // Assert
            foreach (var i in employees)
            {
                Assert.AreEqual(_employee.Name, i.Name);
            }
        }
    }
}