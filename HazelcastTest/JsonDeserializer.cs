﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HazelcastTest
{
    public static class JsonDeserializer<TEntity> where TEntity : class
    {
        public static IEnumerable<TEntity> DeserializeCollection(string jsonCollection)
        {
            return JsonConvert.DeserializeObject<List<TEntity>>(jsonCollection);
        }

        public static TEntity DeserializeEntity(string jsonEntity)
        {
            return JsonConvert.DeserializeObject<TEntity>(jsonEntity);
        }
    }
}
