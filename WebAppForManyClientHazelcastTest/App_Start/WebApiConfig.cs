﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using WebAppForManyClientHazelcastTest.Models;

namespace WebAppForManyClientHazelcastTest
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var builder = new ODataConventionModelBuilder() { ContainerName = "CompanyContainer"};
            builder.EntitySet<Employee>("Employees1");
            builder.Entity<Employee>()
                   .HasKey(x => x.Id);
            builder.Entity<Employee>()
                   .Property(x => x.Name)
                   .IsRequired();
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
