﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using WebAppForManyClientHazelcastTest.Models;

namespace WebAppForManyClientHazelcastTest.Controllers
{
    public class EmployeesController : ApiController
    {
        private readonly IMap<int, Employee> _map;

        public EmployeesController()
        {
            var config = new ClientConfig();
            config.GetNetworkConfig()
                  .AddAddress("104.155.193.4:5701");
            config.GetSerializationConfig()
                  .AddDataSerializableFactory(
                      CompanyDataSerializableFactory.FactoryId,
                      new CompanyDataSerializableFactory());
            var client = HazelcastClient.NewHazelcastClient(config);
            _map = client.GetMap<int, Employee>("employee-map");
        }   

        public IEnumerable<Employee> Get()
        {
            var allEmployeeSet = _map.EntrySet();
            var employeeList = allEmployeeSet.Select(e => e.Value)
                                             .ToList();
            
            return employeeList;
        }

        // GET: api/Employees/5
        public Employee Get(int id)
        {
            return _map.Get(id);
        }

        // POST: api/Employees
        public void Post([FromBody]Employee value)
        {
            _map.Put(value.Id, value);
        }

        // PUT: api/Employees/5
        public void Put(int id, [FromBody]Employee value)
        {
            _map.Put(id, value);
        }

        // DELETE: api/Employees/5
        public void Delete(int id)
        {
            _map.Delete(id);
        }
    }
}
