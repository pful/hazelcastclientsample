﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using WebAppForManyClientHazelcastTest.Models;

namespace WebAppForManyClientHazelcastTest.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using WebAppForManyClientHazelcastTest.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Employee>("Employees1");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class Employees1Controller : ODataController
    {
        private CompanyContext db = new CompanyContext();
        private readonly IMap<int, Employee> _map;

        public Employees1Controller()
        {
            db = new CompanyContext();
            
            // hazelcast connection
            var config = new ClientConfig();
            config.GetNetworkConfig()
                  .AddAddress("104.155.193.4:5701");
            config.GetSerializationConfig()
                  .AddDataSerializableFactory(
                      CompanyDataSerializableFactory.FactoryId,
                      new CompanyDataSerializableFactory());
            var client = HazelcastClient.NewHazelcastClient(config);
            _map = client.GetMap<int, Employee>("employee-map");
        }

        // GET: odata/Employees1
        [EnableQuery]
        public IQueryable<Employee> GetEmployees1()
        {
            return db.Employees;
        }

        // GET: odata/Employees1(5)
        [EnableQuery]
        public SingleResult<Employee> GetEmployee([FromODataUri] int key)
        {
            var employee = _map.Get(key);
            if (employee != null)
                return SingleResult.Create(new[] {employee}.AsQueryable());

            employee = db.Employees.SingleOrDefault(x => x.Id == key);
            if (employee == null)
                throw new NullReferenceException();

            _map.Set(key, employee);
            return SingleResult.Create(new[] {employee}.AsQueryable());
        }

        // PUT: odata/Employees1(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Employee> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Employee employee = db.Employees.Find(key);
            if (employee == null)
            {
                return NotFound();
            }

            patch.Put(employee);

            try
            {
                db.SaveChanges();
                _map.Put(key, employee);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(employee);
        }

        // POST: odata/Employees1
        public IHttpActionResult Post(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Employees.Add(employee);
            db.SaveChanges();
            
            _map.Set(employee.Id, employee);

            return Created(employee);
        }

        // PATCH: odata/Employees1(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Employee> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Employee employee = db.Employees.Find(key);
            if (employee == null)
            {
                return NotFound();
            }

            patch.Patch(employee);

            try
            {
                db.SaveChanges();
                _map.Put(key, employee);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(employee);
        }

        // DELETE: odata/Employees1(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Employee employee = db.Employees.Find(key);
            if (employee == null)
            {
                return NotFound();
            }

            db.Employees.Remove(employee);
            db.SaveChanges();
            _map.Delete(key);

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int key)
        {
            return db.Employees.Count(e => e.Id == key) > 0;
        }
    }
}
