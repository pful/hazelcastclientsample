﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hazelcast.IO.Serialization;

namespace WebAppForManyClientHazelcastTest.Models
{
    public class CompanyDataSerializableFactory : IDataSerializableFactory
    {
        public const int FactoryId = 1;

        public IIdentifiedDataSerializable Create(int typeId)
        {
            switch (typeId)
            {
                case 1:
                    return new Employee();
                default:
                    throw new InvalidOperationException("Unknown class type id");
            }
        }
    }
}