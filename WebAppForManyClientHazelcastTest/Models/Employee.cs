﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Hazelcast.IO;
using Hazelcast.IO.Serialization;

namespace WebAppForManyClientHazelcastTest.Models
{
    public class Employee : IIdentifiedDataSerializable
    {
        [NotMapped] public const int TypeId = 1;

        public int Id { get; set; }
        public string Name { get; set; }

        #region IIdentifiedDataSerializable

        public void ReadData(IObjectDataInput input)
        {
            Id = input.ReadInt();
            Name = input.ReadUTF();
        }

        public void WriteData(IObjectDataOutput output)
        {
            output.WriteInt(Id);
            output.WriteUTF(Name);
        }

        public int GetFactoryId()
        {
            return CompanyDataSerializableFactory.FactoryId;
        }

        public int GetId()
        {
            return TypeId;
        }

        #endregion
    }
}