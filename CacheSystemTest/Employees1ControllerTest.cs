﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Simple.OData.Client;
using WebAppForManyClientHazelcastTest.Models;

namespace CacheSystemTest
{
    [TestClass]
    public class Employees1ControllerTest
    {
        private ODataClient _client;
        private IMap<int, Employee> _map;
        private CompanyContext _db;

        public Employees1ControllerTest()
        {
            var settings = new ODataClientSettings()
                           {
                               BaseUri = new Uri("http://localhost:7131/odata")
                           };

            _client = new ODataClient(settings);

            // hazelcast setting
            var config = new ClientConfig();
            config.GetNetworkConfig()
                  .AddAddress("104.155.193.4:5701");
            config.GetSerializationConfig()
                  .AddDataSerializableFactory(
                      CompanyDataSerializableFactory.FactoryId,
                      new CompanyDataSerializableFactory());
            _map = HazelcastClient.NewHazelcastClient(config)
                                  .GetMap<int, Employee>("employee-map");

            // EF dbcontext
            _db = new CompanyContext();
        }

        /*[TestMethod]
        public void GetAllEmployees()
        {
        }*/

        [TestMethod]
        public void GetEmployeeById_ExistentInCache()
        {
            // Arrange : use seed data
            const string expectedName = "employee1";
            var employee1 = _db.Employees.Find(1);
            _map.Set(1, employee1);

            // Act
            var getAsyncTask = _client.For("Employees1")
                                      .Key(1)
                                      .FindEntryAsync();
            getAsyncTask.Wait();

            // Assert
            Assert.AreEqual(1, getAsyncTask.Result["Id"]);
            Assert.AreEqual(expectedName, getAsyncTask.Result["Name"]);
        }

        [TestMethod]
        public void GetEmployeeById_InexistentInCache()
        {
            // Arrange
            const string expectedName = "employee1";
            _map.Delete(1);

            // Act
            var getAsyncTask = _client.For("Employees1")
                                      .Key(1)
                                      .FindEntryAsync();
            getAsyncTask.Wait();

            // Assert
            Assert.AreEqual(1, getAsyncTask.Result["Id"]);
            Assert.AreEqual(expectedName, getAsyncTask.Result["Name"]);
            Assert.AreEqual(expectedName, _map.Get(1).Name);
        }

        [TestMethod]
        public void CreateNewEmployee()
        {
            // Arrange
            const string expectedName = "new employee";
            var employee = new {Name = expectedName};

            // Act
            var postAsyncTask = _client.For("Employees1")
                                       .Set(employee)
                                       .InsertEntryAsync();
            postAsyncTask.Wait();

            // Assert
            var id = Convert.ToInt32(postAsyncTask.Result["Id"]);
            Assert.IsTrue(id > 0);
            Assert.AreEqual(expectedName, postAsyncTask.Result["Name"]);
            Assert.AreEqual(expectedName, _map.Get(id).Name);
        }

        [TestMethod]
        public void UpdateEmployeeName()
        {
            // Arrange
            const string expectedName = "changed name";
            var employee = new {Name = "name"};
            var postAsyncTask = _client.For("Employees1")
                                       .Set(employee)
                                       .InsertEntryAsync();
            postAsyncTask.Wait();
            var id = Convert.ToInt32(postAsyncTask.Result["Id"]);

            // Act
            _client.For("Employees1")
                   .Key(id)
                   .Set(new {Name = expectedName})
                   .UpdateEntryAsync();

            // Assert
            var foundEmployeeFromDb = _db.Employees.Find(id);
            var foundEmployeeFromHazelcast = _map.Get(id);

            Assert.AreEqual(id, foundEmployeeFromDb.Id);
            Assert.AreEqual(expectedName, foundEmployeeFromDb.Name);
            Assert.AreEqual(id, foundEmployeeFromHazelcast.Id);
            Assert.AreEqual(expectedName, foundEmployeeFromHazelcast.Name);
        }

        [TestMethod]
        public void DeleteEmployee()
        {
            // Arrange
            const string expectedName = "employee for test";
            var employee = new {Name = expectedName};
            var postAsyncTask = _client.For("Employees1")
                                       .Set(employee)
                                       .InsertEntryAsync();
            postAsyncTask.Wait();
            var id = Convert.ToInt32(postAsyncTask.Result["Id"]);

            // Act
            _client.For("Employees1")
                   .Key(id)
                   .DeleteEntryAsync()
                   .Wait();

            // Assert
            Assert.IsNull(_map.Get(id));
            Assert.IsNull(_db.Employees.Find(id));
        }
    }
}